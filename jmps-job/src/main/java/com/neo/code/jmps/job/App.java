package com.neo.code.jmps.job;


import java.lang.reflect.Field;

/**
 * Hello world!
 *
 * @author baiyp
 */
public class App {
    public static void main(String... args) {
        try {
            // get class
            Class<?> clazz = Class.forName("com.neo.code.jmps.core.user.pojo.User");
            System.out.println(clazz.getName());

            // get field
            Field field = clazz.getDeclaredField("name");
            System.out.println(field.getName());

            // new instance
            Object o = clazz.newInstance();

            // set accessible and set field value
            field.setAccessible(true);
            field.set(o, "baiyp");
            System.out.println(o);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
