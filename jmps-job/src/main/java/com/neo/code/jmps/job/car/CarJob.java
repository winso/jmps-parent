package com.neo.code.jmps.job.car;

import com.neo.code.jmps.core.car.pojo.Car;
import com.neo.code.jmps.core.car.service.CarService;

/**
 * @author baiyp
 */
public class CarJob {

    private CarService carService = new CarService();

    public void execute() {
        Car oneCar = carService.getOneCar();
        System.out.println("[car brand]: " + oneCar.getBrand());
    }
}
