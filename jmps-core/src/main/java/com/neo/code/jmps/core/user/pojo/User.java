package com.neo.code.jmps.core.user.pojo;

/**
 * @author baiyp
 */
public class User {

    private String name = "neo";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
