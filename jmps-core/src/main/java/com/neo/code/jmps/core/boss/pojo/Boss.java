package com.neo.code.jmps.core.boss.pojo;

/**
 * @author baiyp
 */
public class Boss {

    private String name = "boss";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
