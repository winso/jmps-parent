package com.neo.code.jmps.core.car.pojo;

/**
 * @author baiyp
 */
public class Car {

    private String brand = "Audi";

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
}
