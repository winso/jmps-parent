package com.neo.code.jmps.web.car;

import com.neo.code.jmps.core.car.pojo.Car;
import com.neo.code.jmps.core.car.service.CarService;

/**
 * @author baiyp
 */
public class CarRest {

    private CarService carService = new CarService();

    public Car getCar() {
        Car oneCar = carService.getOneCar();
        System.out.println("[car brand]: " + oneCar.getBrand());
        return oneCar;
    }
}
